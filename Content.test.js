import React from 'react';
import {render} from 'react-testing-library';

import Content from './content';

test('Content', () => {
	const {debug} = render(<Content content="test" />);

	console.log(debug());

	expect('a').toBe('b');
});
